#!/bin/bash

usage() { echo "Useage: $0 [-p <PATH>] <CORRELATION_ID>"; exit; }

while getopts "p:" opt; do
  case $opt in
    p)
      SRCH_PATH=$OPTARG
      ;;
    \?)
      usage
      ;;
  esac
done
shift $(($OPTIND-1))

CORR_ID=$1

if [ -z $CORR_ID ]; then
  usage
fi

if [ -z $SRCH_PATH ]; then
  SRCH_PATH="."
fi

grep -r "$CORR_ID" $SRCH_PATH | awk 'match($0, /[0-9]+-[0-9]+-[0-9]+[T,_][0-9]+:[0-9]+:[0-9]+\.[0-9]+/) { print substr($0,RSTART,RLENGTH) " " $0}' | sort -k1,1 | awk '{$1=""; print $0}'
