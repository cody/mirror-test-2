#!/usr/bin/ruby

require 'json'
require 'optparse'
require_relative './lib/stats'

DEFAULT_REPORT = :all
REPORTS = {
  worker_stats: "Sidekiq Worker Stats",
}

class SidekiqLogStats < Stats
  def parse_input(data)
    if is_json?(data.first)
      parse_json(data)
    else
      parse_unstructured(data)
    end

  end

  def is_json?(first_line)
    begin
      JSON.parse(first_line, :symbolize_names => true)
      true
    rescue JSON::ParserError
      false
    end
  end

  def parse_json(data)
    results = []

    data.map do |line|
      begin
        event = JSON.parse(line, :symbolize_names => true)

        worker = event[:class]
        duration = event[:duration]
        status = event[:job_status]

        if worker and duration
          results << {worker: worker, duration: duration * 1000.0, status: status}
        end
      rescue JSON::ParserError
      end
    end

    results
  end

  def parse_unstructured(data)
    results = []

    data.each do |line|
      if line =~ /TID-[\S]+\s(\S+)/
        worker = $1
      end

      if line =~ /(done):\s(\d\.\d+)/
        status = $1
        duration = $2.to_f
      elsif line =~ /(fail):\s(\d\.\d+)/
        status = $1
        duration = $2.to_f
      end

      if worker and duration and status
        results << {worker: worker, duration: duration * 1000.0, status: status}
      end
    end

    results
  end

  def parse_time(line)
    if is_json?(line)
      event = JSON.parse(line, :symbolize_names => true)
      time = event[:time]
    else
      if line =~ /^([\S]+)/
        time = $1
      end
    end

    time
  end

  def worker_stats
    @data
      .group_by{|entry| entry[:worker]}
      .map{|worker, entries| {
        worker: worker,
        count: entries.length,
        perc99: percentile(99, durations(entries)).round(2),
        perc95: percentile(95, durations(entries)).round(2),
        stddev: stddev(durations(entries)).round(2),
        max: durations(entries).last.round(2),
        min: durations(entries).first.round(2),
        score: (percentile(99, durations(entries)).round(2) * entries.length).round(1),
        percfail: (entries.select{|entry| entry[:status] != "done"}.length.to_f / entries.length * 100.0).round(2),
      }
    }.sort_by!{|results| results[:score]}.reverse
  end
end

SidekiqLogStats.execute(ARGV[0], REPORTS, DEFAULT_REPORT)
