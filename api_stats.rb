#!/usr/bin/ruby

require 'json'
require 'optparse'
require_relative './lib/stats'

DEFAULT_REPORT = :all
REPORTS = {
  slowest_calls: "Slowest Calls",
  top_paths: "Top Paths by Volume",
  top_err_paths: "Top Paths by 5XX Errors",
  top_routes: "Top Routes by Volume",
  top_err_routes: "Top Routes by 5XX Errors",
  top_users: "Top Active Users",
  top_ips: "Top IP Addresses",
  route_stats: "Route Stats",
}

class ApiJsonLogStats < Stats
  def slowest_calls
    @data
      .sort_by{|entry| entry[:duration]}
      .reverse
      .map{|entry| {
        duration: entry[:duration],
        time: entry[:time],
        correlation_id: entry[:correlation_id],
        username: entry[:username],
        path: entry[:path],
      }
    }.first(@count)
  end

  def top_paths
    @data
      .group_by{|entry| entry[:path] }
      .sort_by{|_, entries| entries.length }
      .reverse
      .map{|path, entries| {count: entries.length, path: path}}
      .first(@count)
  end

  def top_err_paths
    @data
      .select{|entry| entry[:status] >= 500}
      .group_by{|entry| entry[:path] }
      .sort_by{|_, entries| entries.length }
      .reverse
      .map{|path, entries| {count: entries.length, path: path} }
      .first(@count)
  end

  def top_routes
    @data
      .group_by{|entry| entry[:route] }
      .sort_by{|_, entries| entries.length }
      .reverse
      .map{|route, entries| {count: entries.length, route: route}}
      .first(@count)
  end

  def top_err_routes
    @data
      .select{|entry| entry[:status] >= 500}
      .group_by{|entry| entry[:route] }
      .sort_by{|_, entries| entries.length }
      .reverse
      .map{|route, entries| {count: entries.length, route: route} }
      .first(@count)
  end

  def top_users
    @data
      .select{|entry| !entry[:username].nil? }
      .group_by{|entry| entry[:username] }
      .sort_by{|_, entries| entries.length }
      .reverse
      .map{|user, entries| {count: entries.length, user: user} }
      .first(@count)
  end

  def top_ips
    @data
      .select{|entry| !entry[:ip].nil? }
      .group_by{|entry| entry[:ip] }
      .sort_by{|_, entries| entries.length }
      .reverse
      .map{|ip, entries| {count: entries.length, ip: ip} }
      .first(@count)
  end

  def route_stats
    @data
      .group_by{|entry| entry[:route]}
      .map{|route, entries| {
        route: route,
        count: entries.length,
        perc99: percentile(99, durations(entries)).round(2),
        perc95: percentile(95, durations(entries)).round(2),
        stddev: stddev(durations(entries)).round(2),
        max: durations(entries).last.round(2),
        min: durations(entries).first.round(2),
        score: (percentile(99, durations(entries)).round(2) * entries.length).round(1),
        percfail: (entries.select{|entry| entry[:status] >= 500}.length.to_f / entries.length * 100.0).round(2),
      }
    }.sort_by!{|results| results[:score]}.reverse
  end
end

ApiJsonLogStats.execute(ARGV[0], REPORTS, DEFAULT_REPORT)
