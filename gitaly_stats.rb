#!/usr/bin/ruby

require 'json'
require 'optparse'
require_relative './lib/stats'

DEFAULT_REPORT = :all
REPORTS = {
  method_stats: "Gitaly Method Stats",
}

class GitalyLogStats < Stats
  def parse_input(data)
    if is_json?(data.first)
      parse_json(data)
    else
      parse_logfmt(data)
    end

  end

  def is_json?(first_line)
    begin
      JSON.parse(first_line, :symbolize_names => true)
      true
    rescue JSON::ParserError
      false
    end
  end

  def parse_json(data)
    results = []

    data.map do |line|
      begin
        event = JSON.parse(line, :symbolize_names => true)

        method = event[:"grpc.method"]
        duration = event[:"grpc.time_ms"]
        code = event[:"grpc.code"]

        if method and duration
          results << {method: method, duration: duration, code: code}
        end
      rescue JSON::ParserError
      end
    end

    results
  end

  def parse_logfmt(data)
    results = []

    data.each do |line|
      if line =~ /grpc.method=(\w*)/
        method = $1
      end

      if line =~ /grpc.time_ms=([\d\.]*)/
        duration = $1.to_f
      end

      if line =~ /grpc.code=(\w*)/
        code = $1
      end

      if method and duration and code
        results << {method: method, duration: duration, code: code}
      end
    end

    results
  end

  def parse_time(line)
    if is_json?(line)
      event = JSON.parse(line, :symbolize_names => true)
      time = event[:time]
    else
      if line =~ /time=\"([^\s]+Z)\"/
        time = $1
      end
    end

    time
  end

  def method_stats
    @data
      .group_by{|entry| entry[:method]}
      .map{|method, entries| {
        method: method,
        count: entries.length,
        perc99: percentile(99, durations(entries)).round(2),
        perc95: percentile(95, durations(entries)).round(2),
        stddev: stddev(durations(entries)).round(2),
        max: durations(entries).last.round(2),
        min: durations(entries).first.round(2),
        score: (percentile(99, durations(entries)).round(2) * entries.length).round(1),
        percfail: (entries.select{|entry| entry[:code] != "OK"}.length.to_f / entries.length * 100.0).round(2),
      }
    }.sort_by!{|results| results[:score]}.reverse
  end
end

GitalyLogStats.execute(ARGV[0], REPORTS, DEFAULT_REPORT)
